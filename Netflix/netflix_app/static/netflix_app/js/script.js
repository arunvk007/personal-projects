// netflix_app/static/netflix_app/js/script.js

document.addEventListener("DOMContentLoaded", function () {
    const movies = document.querySelectorAll('.movie img');
    const banner = document.querySelector('.banner img');

    // Example: Handle click on a movie - simulate fetching movie details
    movies.forEach(movie => {
        movie.addEventListener('click', () => {
            const movieTitle = movie.alt;
            alert(`Clicked on ${movieTitle}`);

            // Simulate fetching movie details from a server
            fetchMovieDetails(movieTitle)
                .then(details => {
                    // Display the movie details (replace this with your logic)
                    alert(`Movie Details:\n${JSON.stringify(details, null, 2)}`);
                })
                .catch(error => {
                    console.error('Error fetching movie details:', error);
                });
        });
    });

    // Example: Change the header background on scroll
    const header = document.querySelector('.header');

    window.addEventListener('scroll', () => {
        if (window.scrollY > 100) {
            header.style.backgroundColor = '#111';
        } else {
            header.style.backgroundColor = 'transparent';
        }
    });

    // Example: Simulate fetching movie banner dynamically
    fetchMovieBanner()
        .then(bannerSrc => {
            banner.src = bannerSrc;
        })
        .catch(error => {
            console.error('Error fetching movie banner:', error);
        });

    // Simulate fetching movie details from a server
    function fetchMovieDetails(movieTitle) {
        return new Promise((resolve, reject) => {
            // Simulate an asynchronous request to a server
            setTimeout(() => {
                // Replace this with your actual server request logic
                const movieDetails = {
                    title: movieTitle,
                    genre: 'Action',
                    releaseDate: '2023-01-01',
                    rating: 'PG-13',
                };
                resolve(movieDetails);
            }, 1000); // Simulate a 1-second delay
        });
    }

    // Simulate fetching movie banner dynamically
    function fetchMovieBanner() {
        return new Promise((resolve, reject) => {
            // Simulate an asynchronous request to a server
            setTimeout(() => {
                // Replace this with your actual server request logic
                const bannerSrc = '{% static "netflix_app/images/movie_banner.jpg" %}';
                resolve(bannerSrc);
            }, 2000); // Simulate a 2-second delay
        });
    }
});
